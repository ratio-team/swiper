var gulp = require('gulp');
var webpack = require('webpack-stream');

gulp.task('webpack', function() {
  return gulp.src('./demo/scripts.js')
    .pipe(webpack({
      output: {
        filename: 'bundle.js',
      }
    }))
    .pipe(gulp.dest('./demo/dist/'));
});

gulp.task('watch', function() {
    gulp.watch('./**/*.js', ['webpack']);
});

gulp.task('default', ['webpack', 'watch']);

