/***

This slider is very abstract concept. It's not gallery or anything, it's simply a library which takes a container, intercepts gestures and returns callback with "coordinates".

This library doesn't render any images / slides / etc. It returns virtual slide position coordinates and we can interpret them however we want.

Let's say we set slide count to 5. Then output coordinates from the slider are 0 to 4. 0 means we are on the 1st virtual slide. 0.5 means we're between 1st and 2nd. 1 means second etc. We can have < 0 and > 4, which is responsible for bounce effect. We have elastic logarithmic bounce.

What is slide width? Touch space may be much bigger than one slide. Example: We have full width touch space and we see one slide at the center 50% width and two at the sides 25%. In such case moving finger 50% width should move the whole slide.
***/

var Hammer = require("hammerjs");
var $ = require("jquery");
require("gsap");
var VerticalScrollDetector = require("./VerticalScrollDetector.js");

var AbstractSwiper = function(_containerSelector, options) {

	if (typeof options === 'undefined') { options = {} };
	if (typeof options.direction === 'undefined') { options.direction = AbstractSwiper.HORIZONTAL }

	var _count = 1;

	this._relativeX = 0.0; // If we have 4 slides it's number between 0.0 and 3.0 saying exactly in which place gallery is at the moment.
	
	var _isTouched = false;  // true if touch gesture is in progress
	var _isMoving = false;  // true if animation is in progress
	var _isAtPeace = true; // true if at peace - this means that slider is still and there's no touch event active.

	var _animations = [];

	var _panStartX = 0;

	// These 3 variables detects case of multiple and very quick swipes.
	var _swipeTarget = null;
	var _swipeDirection = 1;
	var _swipeTimer = undefined;

	// Sliding parameters
	var _slideWidth; // what is width of one slide

	var $this = this;

	var _targetSlide = 0;

	var _activeSlide = 0; // slide which is currently "the closest to the center", let's call it active.

	var _mc;

	var _animationCurve = Expo.easeOut;
	var _animationTime = 0.8;

	this.setAnimationCurve = function(curve) {
		_animationCurve = curve;
	}

	this.setAnimationTime = function(time) {
		_animationTime = time;
	}


	this.getTargetSlide = function() {
		return _targetSlide;
	}

	this.getActiveSlide = function() {
		return _activeSlide;
	}

	this.setSlideSize = function(slideWidth) {
		_slideWidth = slideWidth;
	}

	this.setCount = function(count) {
		_count = count;
	}

	this.getCount = function() {
		return _count;
	}

	this.isMoving = function() {
		return Math.abs($this._relativeX % 1) > 0.01;
	}

	var _movementCallback;
	var _panStartCallback;
	var _panEndCallback;
	var _activeSlideChangedCallback;

	var _atPeaceCallback;

	var _keyRightCallback;
	var _keyLeftCallback;

	function setAtPeace(status) {
		if (status == _isAtPeace) { return; }
		_isAtPeace = status;

		if (typeof _atPeaceCallback == "function") { _atPeaceCallback(_isAtPeace); }
	}

	this.setAtPeaceCallback = function(callback) {
		_atPeaceCallback = callback;
	}

	this.setKeyRightCallback = function(callback) {
		_keyRightCallback = callback;
	}

	this.setKeyLeftCallback = function(callback) {
		_keyLeftCallback = callback;
	}

	this.setMovementCallback = function(callback) {
		_movementCallback = callback;
	}

	this.setActiveSlideChangedCallback = function(callback) {
		_activeSlideChangedCallback = callback;
	}

	this.setPanStartCallback = function(callback) {
		_panStartCallback = callback;
	}

	this.setPanEndCallback = function(callback) {
		_panEndCallback = callback;
	}

	var _enabled = false;

	this.enable = function() {
		if (_enabled) { return; }
		_enabled = true;

		_mc = new Hammer($(_containerSelector)[0], { domEvents: true });

		var lastMove = 1;
		var swiped = false;

		var hammerDirection = options.direction == AbstractSwiper.HORIZONTAL ? Hammer.DIRECTION_HORIZONTAL : Hammer.DIRECTION_VERTICAL;

		_mc.get('pan').set({ direction: hammerDirection, threshold: 10 });
		_mc.get('swipe').set({ direction: hammerDirection, threshold: 10 });

		function onPanStart(ev) {

			if (ev.distance > 50) {
				// prevents weird bug, when on pan start there's a HUGE distance and huge deltas on Chrome.
				// When we are on slider and we start scrolling in vertical direction (body scroll) starting with touch space of slider, slider gets unexpected panstart and one panleft/panright with HUGE delta.
				return;
			}

			if (!_isTouched) {

				if (_panStartCallback) { _panStartCallback() };

				_isTouched = true;
				swiped = false;

				for(var i = 0; i < _animations.length; i++) {
					_animations[i].kill();
				}

				_animations = [];
				_isMoving = false;

				_panStartX = -$this._relativeX * (_slideWidth);  //$('#wrapper')[0]._gsTransform.x;

				setAtPeace(false);
			}
		}

		_mc.on("pan panup panleft panright pandown panstart panend swipe swipeleft swiperight swipeup swipedown", function(ev) {

			var delta = options.direction == AbstractSwiper.HORIZONTAL ? ev.deltaX : ev.deltaY;

			switch(ev.type) {

				case "swipeleft":
				case "swipeup":

					if (_isTouched) {

						if (_swipeDirection == 1 && _swipeTarget != null) {
							_swipeTarget = Math.min(_swipeTarget + 1, _count - 1);
						}
						else {
							_swipeTarget = Math.min(Math.ceil($this._relativeX), _count - 1);
						}

						_swipeDirection = 1;

						$this.goTo(_swipeTarget);

						clearTimeout(_swipeTimer);
						_swipeTimer = setTimeout(function() { _swipeTarget = null; }, 600);

						swiped = true;
					}

				break;
				case "swiperight":
				case "swipedown":

					if (_isTouched) {

						if (_swipeDirection == 2 && _swipeTarget != null) {
							_swipeTarget = Math.max(_swipeTarget - 1, 0.0);
						}
						else {
							_swipeTarget = Math.max(Math.floor($this._relativeX), 0.0);
						}

						_swipeDirection = 2;
						$this.goTo(_swipeTarget);

						clearTimeout(_swipeTimer);
						_swipeTimer = setTimeout(function() { _swipeTarget = null; }, 600);

						swiped = true;
					}

				break;
				case "panstart":

					// disableScroll();

					// if (!_isTouched) {

					// 	if (_panStartCallback) { _panStartCallback() };

					// 	_isTouched = true;
					// 	swiped = false;

					// 	for(var i = 0; i < _animations.length; i++) {
					// 		_animations[i].kill();
					// 	}

					// 	_animations = [];
					// 	_isMoving = false;

					// 	_panStartX = -$this._relativeX * (_slideWidth);  //$('#wrapper')[0]._gsTransform.x;

					// 	setAtPeace(false);
					// }

				break;

				case "panleft":
					if (VerticalScrollDetector.isScrolling()) { break; } // if body is scrolling then not allow for horizontal movement
				case "panup":
					onPanStart(ev); // onPanStart is on first panleft / panright, because its deferred until treshold is achieved

					if (_isTouched) {
						$this._pan(delta, _panStartX);
						lastMove = 1;
					}

				break;

				case "panright":
					if (VerticalScrollDetector.isScrolling()) { break; } // if body is scrolling then not allow for horizontal movement
				case "pandown":

					onPanStart(ev); // onPanStart is on first panleft / panright, because its deferred until treshold is achieved

					if (_isTouched) {
						$this._pan(delta, _panStartX);
						lastMove = 2;
					}

				break;


				case "panend":

					if (_isTouched) {

						if (_panEndCallback) { _panEndCallback() };

						_isTouched = false;

						if (!swiped) {
							if ($this._relativeX < 0.0) { // top bounce 
								$this.goTo(0.0, Expo.easeOut, _animationTime);
								break;
							}
							else if ($this._relativeX > (_count - 1)) { // bottom bounce
								$this.goTo(_count - 1, Expo.easeOut, _animationTime);
								break;
							}
							else if (lastMove == 1 && $this._relativeX % 1 > 0.33) { // down move
								$this.goTo(Math.ceil($this._relativeX), Expo.easeOut, _animationTime);
							}
							else if (lastMove == 2 && $this._relativeX % 1 < 0.66) { // up move
								$this.goTo(Math.floor($this._relativeX), Expo.easeOut, _animationTime);
							}
							else { // return
								$this.goTo(Math.round($this._relativeX), Expo.easeOut, _animationTime);
							}
						}

						swiped = false;
					}
				break;
			}
		});



	};

	this.enableArrows = function() {

		$(document).keydown(function(a) {

			if (_isTouched) {
				return;
			}

	        switch (a.which) {
	            case 37:
	            	$this.goToPreviousSlide();
					if (_keyLeftCallback) { _keyLeftCallback() };
	                break;
	            case 38:
	                break;
	            case 39:
	            	$this.goToNextSlide();
					if (_keyRightCallback) { _keyRightCallback() };
	            	break;
	            case 40:
	                break;
	            default:
	                return
	        }

		    a.preventDefault();

	    })
	}

	this.disableArrows = function() {
		$(document).unbind('keydown');
	}

	this.isLast = function() {
		return this.getActiveSlide() == (_count - 1);
	}

	this.isFirst = function() {
		return this.getActiveSlide() == 0;
	}

	this.disable = function() {
		if (!_enabled) { return; }
		_enabled = false;

		$(window).unbind('mousewheel DOMMouseScroll');
		_mc.off("pan panup panleft panright pandown panstart panend swipe swipeleft swiperight swipeup swipedown");
	}

	this.goToNextSlide = function(anim, time) {

		if (time == undefined) { time = _animationTime; };
		if (anim === undefined) { anim = _animationCurve; }

		// Don't initialize animations if we're at the end.
		if (Math.abs($this._relativeX - (_count - 1)) < 0.01) {
			return;
		}

		$this.goTo(Math.min(Math.ceil($this._relativeX + 0.5), _count - 1), anim, time);
	}

	this.goToPreviousSlide = function(anim, time) {

		if (time == undefined) { time = _animationTime; };
		if (anim === undefined) { anim = _animationCurve; }

		// Don't initialize animations if we're at the end.
		if (Math.abs($this._relativeX) < 0.01) {
			return;
		}

		$this.goTo(Math.max(Math.floor($this._relativeX - 0.5), 0), anim, time);
	}


	this._pan = function(deltaX, startX) {

		var position = -(startX + deltaX) / _slideWidth;

		if (position < 0) {
			var rest = -position;
			position = - 0.3 * Math.log(rest + 1);
		}

		if (position > (_count - 1)) {
			var rest = position - (_count - 1);
			position = (_count - 1) + 0.3 * Math.log(rest + 1);
		}

		$this._relativeX = position;

		$this._updateRelativeX();
	}


	this._updateRelativeX = function() {

		// TODO - commented out, WARNING

		// if ($this._relativeX < 0.00001 || $this._relativeX > (_count - 1)) {
		// 	return;
		// }

		var rest = $this._relativeX % 1;

		if (rest < 0.01) { // If rest is very small then often we got let's say -.00000001 instead of 0.000000001 and floor and ceil behaves badly.
			var leftIndex = Math.round($this._relativeX);
			var rightIndex = leftIndex + 1;
		} else {
			var leftIndex = Math.floor($this._relativeX);
			var rightIndex = Math.ceil($this._relativeX);
		}

		var coords = this.getCoords();

		var newActiveSlide = Math.round(coords.position);
		if (_activeSlide != newActiveSlide && newActiveSlide >= 0 && newActiveSlide < _count) {
			_activeSlide = newActiveSlide;
			if (_activeSlideChangedCallback) { _activeSlideChangedCallback(newActiveSlide) };
		}

		_movementCallback(coords);
	}

	this.getCoords = function() {

		// TODO - commented out, WARNING
		// if ($this._relativeX < 0 || $this._relativeX > (_count - 1)) {
		// 	return;
		// }

		var rest = $this._relativeX % 1;

		if (rest < 0.01) { // If rest is very small then often we got let's say -.00000001 instead of 0.000000001 and floor and ceil behaves badly.
			var leftIndex = Math.round($this._relativeX);
			var rightIndex = leftIndex + 1;
		} else {
			var leftIndex = Math.floor($this._relativeX);
			var rightIndex = Math.ceil($this._relativeX);
		}

		return {
			position: $this._relativeX,
			prevIndex: leftIndex,
			prevPercent: 1.0 - rest,
			nextIndex: rightIndex,
			nextPercent: rest
		}

		// _movementCallback($this._relativeX, leftIndex, 1.0 - rest, rightIndex, rest);

	}

	this.currentIndex = function() {
		return Math.min(Math.max(0.0, Math.round($this._relativeX)), _count - 1);
	}

	this.normalize = function() {
		this._updateRelativeX();
	}

	this.goToWithoutAnimation = function (index) {
		this.goTo(index, Expo.easeOut, 0);

		// console.log(this._relativeX);
    	// $this._updateRelativeX();
	}

	this.goTo = function(index, anim, time) {

		if (typeof time === "undefined") { time = _animationTime; };
		if (typeof anim === "undefined") { anim = _animationCurve; }

		_targetSlide = index;

		_isMoving = true;

		setAtPeace(false);

	    var anim1 = TweenMax.to(this, time, { _relativeX: index, ease: anim, onUpdate: function() {

	    	$this._updateRelativeX();

	    }, onComplete: function() {

	    	// $this._updateRelativeX();
	    	// console.log('ON COMPLETE');

	    	_isMoving = false;
	    	_animations = [];

	    	_swipeTarget = null; 

			setAtPeace(true);
	    	
	    }});

	    _animations = [anim1];
	}
}

AbstractSwiper.HORIZONTAL = 0;
AbstractSwiper.VERTICAL = 1;

module.exports = AbstractSwiper;


