var AbstractSwiper = require("./AbstractSwiper.js");
var SimpleSwiper = require("./SimpleSwiper.js");

module.exports = {
	AbstractSwiper: AbstractSwiper,
	SimpleSwiper: SimpleSwiper
};


