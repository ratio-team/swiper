var $ = require("jquery");
var AbstractSwiper = require("./AbstractSwiper.js");

// Argument is id of container with slides
var SimpleSwiper = function(commonAncestor) {

	var _this = this;

	commonAncestor = $(commonAncestor);

	var container = commonAncestor.find('.ratio-swiper');
	var clickSpacePrevious = commonAncestor.find('.ratio-swiper-click-space-previous');
	var clickSpaceNext = commonAncestor.find('.ratio-swiper-click-space-next');
	var touchSpace = commonAncestor.find('.ratio-swiper-touch-space');
	var pagerItemTemplate = commonAncestor.find('.ratio-swiper-pager-item');

	var counterAll = commonAncestor.find('.ratio-swiper-counter-all');
	var counterCurrent = commonAncestor.find('.ratio-swiper-counter-current');

	var _counterTransformer = function(number) { return "" + number; }
	this.setCounterTransformer = function(transformer) {
		_counterTransformer = transformer;
	}

	var pagerItems;

	if (touchSpace.length == 0) {
		touchSpace = container;
	}

	var items = container.children();

	AbstractSwiper.call(this, touchSpace);


	var _containerWidth = container.width();

	var _marginInitFunction = function() { return 0; }
	var _marginFunction = function() { return 0; }
	var _widthFunction = function() { return _containerWidth; }
	var _slideWidthFunction = function() { return _marginFunction() + _widthFunction(); }
	
	this.setMarginInit = function(callback) {
		_marginInitFunction = callback;
	}

	this.setMargin = function(callback) {
		_marginFunction = callback;
	}

	this.setWidth = function(callback) {
		_widthFunction = callback;
	}

	function positionElements() {
		_containerWidth = container.width();
		
		items.each(function(index) {

			$(this).css({
				position: "absolute",
				top: 0,
				left: _marginInitFunction(),
				width: _widthFunction(),
				height: "100%",
				"will-change": "transform"
				// transform: "translateX(" + (index - _this.getActiveSlide()) * (_widthFunction() + _marginFunction()) + ")"
			});
		});
	}

	this.setCount(items.length);
	this.setSlideSize(_slideWidthFunction());

	function onMove(coords) {
		items.each(function(index) {

			var x = -(coords.position - index) * _slideWidthFunction();
			$(this).css('transform', 'translateX(' + x + 'px)');

			// TweenMax.set($(this), { x: -(coords.position - index) * _slideWidthFunction() });
		});

		_movementCallback(coords);
	}

	function updateActive(i) {
		clickSpaceNext.addClass('active');
		clickSpacePrevious.addClass('active');

		if (_this.isFirst()) {
			clickSpacePrevious.removeClass('active');
		}
		if (_this.isLast()) {
			clickSpaceNext.removeClass('active');
		}

		// pager items
		pagerItems.removeClass('active');
		pagerItems.eq(i).addClass('active');

		// counters
		counterCurrent.html(_counterTransformer(i + 1));
		counterAll.html(_counterTransformer(items.length));
	}

	clickSpaceNext.click(function(e) {
		e.preventDefault();

		if ($(this).hasClass('active')) {
			_this.goToNextSlide();
		}
	});

	clickSpacePrevious.click(function(e) {
		e.preventDefault();

		if ($(this).hasClass('active')) {
			_this.goToPreviousSlide();
		}
	});

	// Pager items
	if (pagerItemTemplate) {
		for(var i = 0; i < items.length - 1; i++) {
			var pagerItem = pagerItemTemplate.clone();
			pagerItemTemplate.after(pagerItem);
		}
	}

	pagerItems = commonAncestor.find('.ratio-swiper-pager-item');
	pagerItems.each(function(index) {
		$(this).click(function(e) {
			e.preventDefault();
			_this.goTo(index);
		})
	});


	// this.superSetMovementCallback

	this.setMovementCallback(onMove);
	this.setActiveSlideChangedCallback(function(i) {
		updateActive(i);
		_activeSlideChangedCallback(i);
	});

	var _movementCallback = function() {};
	var _activeSlideChangedCallback = function() {};

	this.setMovementCallback = function(callback) {
		_movementCallback = callback;
	}

	this.setActiveSlideChangedCallback = function(callback) {
		_activeSlideChangedCallback = callback;
	}

	this.init = function() {
		positionElements();
		onMove(_this.getCoords());

		updateActive(0);

		_this.enable();
	}

	var oldNormalize = this.normalize;

	this.normalize = function() {
		positionElements();
		oldNormalize.call(this);
	}

	$(window).resize(function() {
		_this.normalize();
	});

}

module.exports = SimpleSwiper;
