var $ = require("jquery");
require("gsap");

var RatioSwiper = require("../index.js");
var AbstractSwiper = RatioSwiper.AbstractSwiper;
var SimpleSwiper = RatioSwiper.SimpleSwiper;

$(document).ready(function() {

	// Horizontal swiper
	var swiperH = new AbstractSwiper('#swiper-horizontal');

	var width = $('#swiper-horizontal').width();

	swiperH.setCount(4);
	swiperH.setSlideSize(width);

	var slidesH = $('#swiper-horizontal .slide');
	swiperH.setMovementCallback(function(coords) {
		TweenMax.set(slidesH, { x: -width * coords.position });
	});

	swiperH.enable();


	// Vertical swiper
	var swiperV = new AbstractSwiper('#swiper-vertical', { direction: AbstractSwiper.VERTICAL });

	var height = $('#swiper-vertical').height();

	swiperV.setCount(4);
	swiperV.setSlideSize(height);

	var slidesV = $('#swiper-vertical .slide');
	swiperV.setMovementCallback(function(coords) {
		TweenMax.set(slidesV, { y: -height * coords.position });
	});

	swiperV.enable();

	// Horizontal simple swiper
	function pad(num, size) {
	    var s = "000000000" + num;
	    return s.substr(s.length-size);
	}

	var simpleSwiper = new SimpleSwiper('#simple-swiper-horizontal');
	simpleSwiper.setCounterTransformer(function(n) {
		return pad(n, 2);
	});
	simpleSwiper.init();
	
});
